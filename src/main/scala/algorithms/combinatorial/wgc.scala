package algorithms.combinatorial

import scala.collection.immutable.Queue

/**
 * A wolf, a goat and a cabbage are staying on the left bank of a river and want to cross on the right bank.
 * There is a boat that can pass at least one passenger and at most two passengers from one side to another.
 * The wolf and the goat or the goat and the cabbage cannot be left alone on a bank as something bad may happen.
 * What are all the possibilities to do the crossing?
 *
 * Note: a crossing leg or, shorter, leg means a single passage from one side to the other. A crossing is a list of legs 
 * that ends with all our travellers on the right side of the river.
 */

enum Habitant:
    case Wolf, Goat, Cabbage

 /**
  * A bank of the river has a set of habitants.
  */   
case class Bank(habitants: Set[Habitant]):
    def departure(travellers: Set[Habitant]): Bank =
        copy(
            habitants = habitants.diff(travellers)
        )
    def arrival(travelers:Set[Habitant]): Bank =
        copy(
            habitants = habitants.union(travelers)
        )

enum BoatPosition:
    case LeftBank, RightBank

/**
 * Description of the river at one point in time. The description includes the habitants on the both banks
 * and the position of the boat.
 */        
case class River(left: Bank, right: Bank, boat: BoatPosition)

def noEventsOnBank(s: Bank): Boolean =
    s.habitants != Set(Habitant.Wolf, Habitant.Goat) || s.habitants != Set(Habitant.Goat, Habitant.Cabbage)

def selectTravelers(s: Bank): List[Set[Habitant]] = 
    s.habitants.subsets.filter(candidates =>
        candidates.size >= 1 && candidates.size <= 2 && noEventsOnBank(s.departure(candidates))
    ).toList

/**
 * Searches all possible crossing legs between a departure and an arrival.
 * 
 * @param d departure bank
 * @param a arrival bank
 * 
 * @return a list of tuples, one for each possible crossing leg, representing departure and arrival banks after the leg is done
 */      
def possibleLegs(d: Bank, a: Bank): List[(Bank, Bank)] =
    selectTravelers(d).map(travellers => (d.departure(travellers), a.arrival(travellers)))

/**
 * Searches all the ways to perform a crossing leg for a specific placement of the river habitants and of the boat.
 * 
 * @param r placement of the river habitants and of the boat
 * 
 * @return a list of elements describing how river will look like after completing a crossing leg, one element for each possible crossing leg
 **/    
def riverAfterOneLeg(r: River): List[River] =
    def riverLeftToRight(r: River): List[River] = possibleLegs(r.left, r.right)
      .map(trip =>
          val (departureAfterTrip, arrivalAfterTrip) = trip
          River(departureAfterTrip, arrivalAfterTrip, BoatPosition.RightBank)
      )
    def riverRightToLeft(r: River): List[River] = possibleLegs(r.right, r.left)
      .map(trip =>
          val (departureAfterTrip, arrivalAfterTrip) = trip
          River(arrivalAfterTrip, departureAfterTrip, BoatPosition.LeftBank)
      )
    r.boat match
        case BoatPosition.LeftBank => riverLeftToRight(r)
        case BoatPosition.RightBank => riverRightToLeft(r)

def allOnRightBank(r: River): Boolean = r.right.habitants == Set(Habitant.Wolf, Habitant.Goat, Habitant.Cabbage)

/**
 * A crossing is a list of descriptions of the river with the most recent description first and the initial description of the river
 * as the last element in the list
 **/
type Crossing = List[River]

enum LegDirection:
    case LeftToRight
    case RightToLeft
    override def toString: String = this match {
        case LeftToRight => s"->"
        case RightToLeft => s"<-"
    }

/**
 * Searchs all ways to complete an already started crossing to the right shore.
 * 
 * @param rs the already done part of the crossing. Current placement of the inhabitants on the shores
 * of the river can be found as the first element.
 * 
 * @return all ways to complete the started crossing with all habitants reaching the right shore.
 */    
def searchCrossingToRightBank(rs: Crossing): List[Crossing] =
    if allOnRightBank(rs.head) then List(rs)
    else 
        for 
            riverAfterTrip <- riverAfterOneLeg(rs.head)
            if !rs.contains(riverAfterTrip)
            crossing <- searchCrossingToRightBank(riverAfterTrip::rs)
        yield
            crossing

/**
 * Searches the shortest crossing to the right shore starting from a queue of in progress crossings.
 * 
 * @param inProgress queue of already started crossings ordered by the number of already executed legs.
 * The shortest crossing will be extracted first.
 * 
 * @return A crossing that completes one of the in progress crossings and has a minimum number of legs.
 **/            
def searchShortestCrossingToRightBank(inProgress: Queue[Crossing]): Crossing = 
    val (crossing, otherCrossings) = inProgress.dequeue
    if allOnRightBank(crossing.head) then crossing
    else searchShortestCrossingToRightBank(otherCrossings.enqueueAll(riverAfterOneLeg(crossing.head).map(r => r :: crossing)))

case class Leg(direction: LegDirection, travelers: Set[Habitant]):
    override def toString: String = s"[${travelers.map(t => t.toString).reduce((t1, t2) => s"$t1, $t2")} $direction]"

/**
 * Transforms a crossing represented as a list of descriptions of the river into a list of crossing legs.
 * 
 * @param rs the sequence of descriptions of the river that are still to be converted into crossing legs
 * @param legs the list of the crossing lengs that corresponds to the part of the crossing that was already converted.
 * 
 * @return the list of crossing legs, the first the leg in the list the first leg of the journey and the last leg in the
 * list is the last leg of the journey.
 **/ 
def crossingAsLegs(rs: Crossing, legs: List[Leg]): List[Leg] = rs match
    case afterLeg::initial::otherDescriptionsBefore =>
        val leg = initial.boat match
            case BoatPosition.LeftBank => Leg(LegDirection.LeftToRight, initial.left.habitants.diff(afterLeg.left.habitants))
            case BoatPosition.RightBank => Leg(LegDirection.RightToLeft, initial.right.habitants.diff(afterLeg.right.habitants))
        crossingAsLegs(initial::otherDescriptionsBefore, leg::legs)
    case _ => legs

def riverCrossing = searchCrossingToRightBank(
        List(
            River(
                left = Bank(Set(Habitant.Wolf, Habitant.Goat, Habitant.Cabbage)), 
                right = Bank(Set.empty),
                boat = BoatPosition.LeftBank
            )
        )
    ).map(crossingAsLegs(_, List.empty))

def riverShortestCrossing = 
    val crossing = searchShortestCrossingToRightBank(
        Queue(
            List(
                    River(
                        left = Bank(Set(Habitant.Wolf, Habitant.Goat, Habitant.Cabbage)), 
                        right = Bank(Set.empty),
                        boat = BoatPosition.LeftBank
                    )
                )   
        )
    )
    crossingAsLegs(crossing, List.empty)
