import algorithms.combinatorial.riverCrossing
import algorithms.combinatorial.riverShortestCrossing

val crossings = riverCrossing

/**
 * Crossings ordered by the number of trips and represented as a string
 */
val crossingsAsString = crossings
  .sortWith((c1, c2) => c1.size < c2.size)
  .map(c => s"\n$c")
  .reduce((c1, c2) => s"$c1, $c2")

val shortestCrossing = riverShortestCrossing
